/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {
	BASKET(final Place place1, final int NoTeamMembers, final String actualName), VOLLEY, TENNIS, BIKE, F1, MOTOGP, SOCCER;
    /*
     * TODO
     * 
     * Declare the following sports:
     * 
     * - basket
     * 
     * - volley
     * 
     * - tennis
     * 
     * - bike
     * 
     * - F1
     * 
     * - motogp
     * 
     * - soccer
     * 
     */

	private int teamMember;
	private String name;
	private Place place;
	
    /*
     * TODO
     * 
     * [FIELDS]
     * 
     * Declare required fields
     */

	Sport(final Place place, final int NoTeamMembers, final String actualName) {
		this.place1 = place;
		this.teamMember = NoTeamMembers;
		this.name = actualName;
	}
    /*
     * 
     * TODO
     * 
     * [CONSTRUCTOR]
     * 
     * Define a constructor like this:
     * 
     * - Sport(final Place place, final int noTeamMembers, final String actualName)
     */

	public boolean isIndividualSport() {
		return this.teamMember == 1; 
		
	}
	
	public boolean isIndoorSport() {
		return this.place1 == Place.INDOOR;
	}
	
	public Place getPlace() {
		return this.place1;
	}
	
	public String toString() {
		System.out.println("");
	}
    /*
     * TODO
     * 
     * [METHODS] To be defined
     * 
     * 
     * 1) public boolean isIndividualSport()
     * 
     * Must return true only if called on individual sports
     * 
     * 
     * 2) public boolean isIndoorSport()
     * 
     * Must return true in case the sport is practices indoor
     * 
     * 
     * 3) public Place getPlace()
     * 
     * Must return the place where this sport is practiced
     * 
     * 
     * 4) public String toString()
     * 
     * Returns the string representation of a sport
     */
}
