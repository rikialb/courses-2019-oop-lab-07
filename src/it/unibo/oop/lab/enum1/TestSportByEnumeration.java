package it.unibo.oop.lab.enum1;

import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
    	
    	SportSocialNetworkUserImpl<User> dilo = new SportSocialNetworkUserImpl<User>("Daniele", "Di_lillo", "DOUBLE_D");
    	SportSocialNetworkUserImpl<User> brighi = new SportSocialNetworkUserImpl<User>("Andrea", "Brighi", "pls");
    	SportSocialNetworkUserImpl<User> me = new SportSocialNetworkUserImpl<User>("Riccardo", "Albertini", "bestest");
    	
    	dilo.addSport(Sport.BASKET);
    	dilo.addSport(Sport.F1);
    	
    	System.out.println("Alonso practices F1: " + dilo.hasSport(Sport.F1));
        System.out.println("Alonso does not like volley: " + !dilo.hasSport(Sport.VOLLEY));

      
        brighi.addSport(Sport.BIKE);
        brighi.addSport(Sport.F1);
        brighi.addSport(Sport.MOTOGP);

        System.out.println("Cassani has been a professional biker: "
                + brighi.hasSport(Sport.BIKE));
        System.out.println("Cassani does not like soccer: " + !brighi.hasSport(Sport.SOCCER));

      
        me.addSport(Sport.F1);
        me.addSport(Sport.BASKET);

        System.out.println("Bernie's the boss when it comes to F1: "
                + me.hasSport(Sport.F1));
        System.out.println("Bernie does love playing also basket: "
                + me.hasSport(Sport.BASKET));
    }

}
